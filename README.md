# simple-3coil

## Part list

- 3x A3144 Hall Effect Sensor
- 3x IRF9520 P-Channel MOSFET
- 1x 10MM OD / 3MM ID Radial Ball Bearing
- 3x 1N4001 Diodes
- 3x 5k Resistors
- 4x 2-Pin Terminal Blocks
- 3x 3-Pin Terminal Blocks
- 4x 10x5MM Neodymium Magnets
- Misc. Nuts/Bolts
- 28AWG Enameled Magnet Wire
- 70x32mm Prototyping Board

## Pictures

![Angled View](/img/motor-angle-view.jpg "Angled View")

![Top View](/img/motor-top-view.jpg "Top View")