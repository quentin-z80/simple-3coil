EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:IRF9540N Q1
U 1 1 60179926
P 2350 2650
F 0 "Q1" H 2555 2604 50  0000 L CNN
F 1 "IRF9540N" H 2555 2695 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2550 2575 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf9540n.pdf" H 2350 2650 50  0001 L CNN
	1    2350 2650
	-1   0    0    1   
$EndComp
$Comp
L Device:L_Core_Iron L1
U 1 1 6017AA46
P 1850 3400
F 0 "L1" V 1669 3400 50  0000 C CNN
F 1 "L_Core_Iron" V 1760 3400 50  0000 C CNN
F 2 "" H 1850 3400 50  0001 C CNN
F 3 "~" H 1850 3400 50  0001 C CNN
	1    1850 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 6017C0CD
P 3200 1750
F 0 "R1" H 3270 1796 50  0000 L CNN
F 1 "R" H 3270 1705 50  0000 L CNN
F 2 "" V 3130 1750 50  0001 C CNN
F 3 "~" H 3200 1750 50  0001 C CNN
	1    3200 1750
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4001 D1
U 1 1 6017C73A
P 1850 3050
F 0 "D1" H 1850 2833 50  0000 C CNN
F 1 "1N4001" H 1850 2924 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 1850 2875 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 1850 3050 50  0001 C CNN
	1    1850 3050
	-1   0    0    1   
$EndComp
$Comp
L Sensor_Magnetic:A1301EUA-T U1
U 1 1 6018196B
P 2800 2000
F 0 "U1" H 2571 2046 50  0000 R CNN
F 1 "A3144" H 2571 1955 50  0000 R CNN
F 2 "" H 2800 1650 50  0001 L CIN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/A3141-2-3-4-Datasheet.ashx" H 2700 2000 50  0001 C CNN
	1    2800 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery BT1
U 1 1 6018335F
P 1150 2000
F 0 "BT1" H 1258 2046 50  0000 L CNN
F 1 "Battery" H 1258 1955 50  0000 L CNN
F 2 "" V 1150 2060 50  0001 C CNN
F 3 "~" V 1150 2060 50  0001 C CNN
	1    1150 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2650 3200 2650
Wire Wire Line
	3200 2650 3200 2000
Wire Wire Line
	3200 2000 3100 2000
Wire Wire Line
	3200 1600 3200 1500
Wire Wire Line
	3200 1500 2700 1500
Wire Wire Line
	2700 1500 2700 1600
Wire Wire Line
	2700 1500 2250 1500
Wire Wire Line
	1150 1500 1150 1800
Connection ~ 2700 1500
Wire Wire Line
	2250 2450 2250 1500
Connection ~ 2250 1500
Wire Wire Line
	2250 1500 1150 1500
Wire Wire Line
	2000 3050 2250 3050
Wire Wire Line
	2250 3050 2250 2850
Wire Wire Line
	2000 3400 2250 3400
Wire Wire Line
	2250 3400 2250 3050
Connection ~ 2250 3050
Wire Wire Line
	1700 3400 1150 3400
Wire Wire Line
	1150 2200 1150 3050
Wire Wire Line
	1700 3050 1150 3050
Connection ~ 1150 3050
Wire Wire Line
	1150 3050 1150 3400
Wire Wire Line
	2700 2400 2700 3600
Wire Wire Line
	2700 3600 1150 3600
Wire Wire Line
	1150 3600 1150 3400
Connection ~ 1150 3400
Wire Wire Line
	3200 1900 3200 2000
Connection ~ 3200 2000
Wire Notes Line
	1550 1450 3400 1450
Wire Notes Line
	3400 1450 3400 3700
Wire Notes Line
	3400 3700 1550 3700
Wire Notes Line
	1550 3700 1550 1450
Text Notes 3200 3900 0    100  ~ 0
X3
$EndSCHEMATC
